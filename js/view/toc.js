mhn.toc = new function()
{
	var self = this;
	this.tree = null;
	
	mhn.core.onLoad(function() { self.initialize(); });
	
	this.initialize = function()
	{
		this.tree = new mhn.Tree("foldersContainer");
		
		for(var i=0;i<mhn.folders.rootTitles.length;i++){
			var rootFolder = mhn.folders.getFolderByTitle(mhn.folders.rootTitles[i]);
			this.generateFolderItem(this.tree,rootFolder);
		}
		
		$(mhn.calendar).bind("dragend",function(event,day){
			if(self.current!=null){
					self.add(self.current,day);
				mhn.core.keyCode = "";
			}
			self.current=null;
		});
	}
	
	this.add = function(task,day)
	{
		var timelog = {
			taskId:task.id,
			userId:mhn.contacts.me.id,
			hours:8
		};
		timelog.trackedDate = mhn.timelogs.convertDate(new Date(mhn.calendar.calendar.currentDate.getFullYear(),mhn.calendar.calendar.currentDate.getMonth(),day.day));
		
		mhn.splash.wait();
		mhn.timelogs.add(timelog,
		function(){
			var folder = mhn.folders.getFolderById(task.parentIds[0]);
			day.cell.appendChild(mhn.calendar.createTimelog(
				timelog,
				folder,
				task
			));
			mhn.splash.wake();
		},
		function(error){
			mhn.splash.show(mhn.splash.ERROR, "Error add timelog", 0, false);
		});
	}
	
	this.generateFolderItem = function(parent,folder)
	{
		
		if(!this.containsTask(folder))
		{
			return;
		}
		
		var content = document.createElement('SPAN');
		content.className = "folder";
		var label = document.createElement('SPAN');
		label.innerHTML = folder.title;
		content.appendChild(label);
		
		var item = new mhn.TreeItem(
		{
			'data': folder, 
			'content': content,
			'open': false,
			'children': []
		});
		
		parent.addChild(item);
		
		
		if(folder.childIds && folder.childIds.length>0)
		{
			for(var i=0;i<folder.childIds.length;i++){
				var child = mhn.folders.getFolderById(folder.childIds[i]);
				if(child)
					this.generateFolderItem(item,child);
				else
					mhn.status.log("Folder id not found:"+folder.childIds[i],"ERROR");
			}
		}
		
		if(folder.tasks && folder.tasks.length>0)
		{
			for(var i=0;i<folder.tasks.length;i++){
				this.generateTaskItem(item,folder.tasks[i]);
			}
		}
		
	}
	
	this.containsTask = function(folder)
	{
		if(!folder)
			return false;
		if(folder.tasks && folder.tasks.length>0)
			return true;
		else if(folder.childIds && folder.childIds.length>0)
		{
			for(var i=0;i<folder.childIds.length;i++){
				var child = mhn.folders.getFolderById(folder.childIds[i]);
				if(child){
					var contains = this.containsTask(child);
					if(contains) return true;
				}
				
			}
		}
		
		return false;
	}
	
	this.generateTaskItem = function(parent,task)
	{
		var status = mhn.tasks.getStatus(task);
		
		var content = document.createElement('SPAN');
		content.className = "task";
		if(status)
			content.style.color = status.color;
		content.draggable = true;
		
		var label = document.createElement('SPAN');
		label.innerHTML = task.title;
		content.appendChild(label);
			
		var item = new mhn.TreeItem(
		{
			'data': task, 
			'content': content,
			'open': false,
			'children': []
		});
		
		content.addEventListener('dragstart', function(){
			self.current = task;
		}, false);
			
		parent.addChild(item);
		
		if(task.subTaskIds && task.subTaskIds.length>0){
			for(var i=0;i<task.subTaskIds.length;i++){
				var subTask = mhn.tasks.getTaskById(task.subTaskIds[i]);
				if(subTask!=null)
					this.generateTaskItem(item,subTask);
				else
					console.log("Subtask not found "+task.subTaskIds[i]);
			}
		}
	}
}