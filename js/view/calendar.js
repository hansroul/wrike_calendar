mhn.calendar = new function()
{
	var self = this;
	
	this.month = ["January","February","March","April","May","June","July","August","September","October","November","December"];
	
	this.calendar = null;
	this.current;
	this.container;
	
	mhn.core.onLoad(function() { self.initialize(); });
	
	this.initialize = function()
	{
		this.container = document.getElementById("calendar"); 
		$(mhn.core).bind("onResize",function(event) { self.onResize(); });
		
		var now = new Date();
		this.initializeCalendar(now);
	}
	
	this.previous = function()
	{
		this.clear();
		var previousMonthDate = new Date(this.calendar.currentDate.getFullYear(), this.calendar.currentDate.getMonth() - 1);
		this.initializeCalendar(previousMonthDate);
	}
	
	this.next = function()
	{
		this.clear();
		var nextMonthDate = new Date(this.calendar.currentDate.getFullYear(), this.calendar.currentDate.getMonth() + 1);
		this.initializeCalendar(nextMonthDate);
	}
	
	this.initializeCalendar = function(now)
	{
		this.calendar = new mhn._calendar(document.getElementById("calendar"));
		///var now = new Date();
		this.calendar.setDate(now);
		
		$(this.calendar).bind("dragend",function(event,day){
			$(self).trigger('dragend',day);
			if(self.current!=null){
				self.current.element.style.opacity = 1.0;
				if(mhn.core.keyCode == 17)
					self.copy(self.current,day);
				else
					self.move(self.current,day);
				mhn.core.keyCode = "";
			}
			self.current=null;
		});
		
		var month = this.month[now.getMonth()];

		document.getElementById("calendarMonth").innerHTML = month+" "+now.getFullYear();
		
		var monthDays = this.calendar.currentDate.monthDays();
		for(var i=1;i<=monthDays;i++)
		{
			var date = new Date(this.calendar.currentDate.getFullYear(),this.calendar.currentDate.getMonth(),i);
			var timelogs = mhn.timelogs.getTimeLogs(date);
			if(timelogs.length>0)
			{
				var day = this.calendar.getDay(i);
				for(var t=0;t<timelogs.length;t++)
				{
					var task = mhn.tasks.getTaskById(timelogs[t].taskId);
					var folder = null;
					if(task != null && task.parentIds.length > 0)
						folder = mhn.folders.getFolderById(task.parentIds[0]);
					var timeLogElement = this.createTimelog(timelogs[t],folder,task);
					day.cell.appendChild(timeLogElement);
				}
			}
		}
	}
	
	this.clear = function()
	{
		document.getElementById("calendarMonth").innerHTML = "";
		while (this.container.firstChild) {
			this.container.removeChild(this.container.firstChild);
		}
	}
	
	this.copy = function(timelogData,day)
	{
		
		var timelog = {};
		for(var key in timelogData.timelog){
			timelog[key]=timelogData.timelog[key];
		}
		timelog.trackedDate = mhn.timelogs.convertDate(new Date(this.calendar.currentDate.getFullYear(),this.calendar.currentDate.getMonth(),day.day));
		
		mhn.splash.wait();
		mhn.timelogs.add(timelog,
		function(){
			day.cell.appendChild(self.createTimelog(
				timelog,
				timelogData.folder,
				timelogData.task
			));
			mhn.splash.wake();
		},
		function(error){
			mhn.splash.show(mhn.splash.ERROR, "Error copy timelog", 0, false);
		});
	
	}
	
	this.move = function(timelogData,day)
	{
		timelogData.element.parentElement.removeChild(timelogData.element);
		timelogData.timelog.trackedDate = mhn.timelogs.convertDate(new Date(this.calendar.currentDate.getFullYear(),this.calendar.currentDate.getMonth(),day.day));
		
		
		mhn.splash.wait();
		mhn.timelogs.update(timelogData.timelog,
		function(){
			day.cell.appendChild(self.createTimelog(
					timelogData.timelog,
					timelogData.folder,
					timelogData.task
				));
			mhn.splash.wake();
		},
		function(error){
			mhn.splash.show(mhn.splash.ERROR, "Error move timelog", 0, false);
		});
		
		
	}
	
	this.createTimelog = function(timelog,folder,task)
	{
		var div = document.createElement("div");
		div.className = "timelog";
		div.draggable = true;
		
		var timeDiv = document.createElement("div");
		timeDiv.className = "time";
		timeDiv.innerHTML = timelog.hours;
		
		var folderTaskDiv = document.createElement("div");
		
		var folderDiv = document.createElement("div");
		folderDiv.className = "folder";
		folderDiv.innerHTML = folder ? folder.title : timelog.id;
		
		var taskDiv = document.createElement("div");
		taskDiv.className = "task";
		taskDiv.innerHTML = task ? task.title : "";
		
		folderTaskDiv.appendChild(folderDiv);
		folderTaskDiv.appendChild(taskDiv);
		
		div.appendChild(folderTaskDiv);
		div.appendChild(timeDiv);
		
		$(div).click(function(){
			mhn.popup.open(div,timeDiv,timelog,folder,task);
		});
		
		var data = {
			timelog:timelog,
			folder:folder,
			task:task,
			element:div
		};
		
		div.addEventListener('dragstart', function(){
			this.style.opacity = '0.4';
			self.current = data;
			}, false);
			
		return div;
	}
	
	
	this.getElementCell = function(element)
	{
		if(hasClassName(element,"cell"))
		{
			return element;
		}
		else if(element.parentElement!=null)
		{
			return this.getElementCell(element.parentElement);
		}
			
		return null;
	}
	
	this.onResize = function()
	{
	}
}

mhn._calendar = function(container)
{
	var self = this;
	
	this.__titlesHeaders = ["LUNDI","MARDI","MERCREDI","JEUDI","VENDREDI","SAMEDI","DIMANCHE"];	
	this.__table;
	this.__header;
	this.days;
	
	this.currentDate;
	
	this.initialize = function(container)
	{
		this.__table = document.createElement("div");
		this.__table.className = "table";
		
		this.__createHeader();
		
		container.appendChild(this.__table);	
	}
	
	this.__createHeader = function()
	{
		this.__header = document.createElement("div");
		this.__header.className = "row header";
		
		this.__table.appendChild(this.__header);
		for(var i=0;i<this.__titlesHeaders.length;i++)
		{
			var title = document.createElement("div");
			title.className = "cell";
			title.innerHTML = this.__titlesHeaders[i];
		
			this.__header.appendChild(title);
		}
	}
	
	this.__createDays = function()
	{
		var date = new Date(this.currentDate.getFullYear(), this.currentDate.getMonth(), 1);
		var columnIndex = date.getDay();
		if(columnIndex == 0)
			columnIndex = 6;
		else 
			columnIndex--;
		
		var monthDays = date.monthDays();
		var row;
		
		this.days = [];
		
		if(columnIndex>0)
		{
			row = document.createElement("div");
			row.className = "row";
			this.__table.appendChild(row);
			
			for(var i=0;i<columnIndex;i++)
			{
				var cell = document.createElement("div");
				cell.className = "cell empty";
			
				row.appendChild(cell);
			}
		}
		
		for(var i=0;i<monthDays;i++)
		{
			if(columnIndex % 7 == 0)
			{
				row = document.createElement("div");
				row.className = "row";
				this.__table.appendChild(row);
				columnIndex=0;
			}
			
			var cell = document.createElement("div");
			cell.className = "cell day";
			cell.appendChild(this.__createDiv(i+1));
			
			this.__addDragEvent(cell);
			
			this.days.push({cell:cell,data:{},day:(i+1)});
		
			row.appendChild(cell);
			columnIndex++;
		}
		
		for(var i=columnIndex;i<7;i++){
			var cell = document.createElement("div");
			cell.className = "cell empty";
			row.appendChild(cell);
		}
		
	}
	
	this.__addDragEvent = function(div)
	{
		div.addEventListener('dragenter', function(e){
			if(self.currentDragItem!=null)
				removeClassName(self.currentDragItem,'over');
			self.currentDragItem = div;
			addClassName(div,'over');
		}, false);
		div.addEventListener('dragover', function(e){
			if (e.preventDefault) {
				e.preventDefault(); // Necessary. Allows us to drop.
			}
			if(mhn.core.keyCode == 17)
				e.dataTransfer.dropEffect = 'copy';
			else
				e.dataTransfer.dropEffect = 'move';
		}, false);
		div.addEventListener('dragleave', function(e){
		}, false);
		div.addEventListener('drop', function(e){
			if (e.stopPropagation) {
				e.stopPropagation(); // stops the browser from redirecting.
			}
			if(self.currentDragItem!=null)
				removeClassName(self.currentDragItem,'over');
			var day = self.getDayByCell(self.currentDragItem);
			$(self).trigger('dragend',day);
			return false;
		}, false);
		div.addEventListener('dragend', function(){
		}, false);
	}
	
	this.getDay = function(day)
	{
		return this.days[day-1];
	}
	
	this.getDayByCell = function(cell)
	{
		for(var i=0;i<this.days.length;i++)
		{
			if(this.days[i].cell === cell)
				return this.days[i];
		}
		return null;
	}
	
	this.setDate = function(date)
	{
		this.currentDate = date;
		this.clear();
		this.__createDays();
	}
	
	this.__createDiv = function(innerHTML)
	{
		var div = document.createElement("div");
		div.innerHTML = innerHTML;
		return div;
	}
		
	this.clear = function(){}
	this.initialize(container);
}