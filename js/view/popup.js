mhn.popup = new function()
{
	var self  =this;
	
	this.isOpen = false;
	this.popup;
	this.select;
	this.currentTimelog;
	this.currentElementTimelog;
	this.currentElementTime;
	
	mhn.core.onLoad(function() { self.initialize(); });
	
	this.initialize = function()
	{
		this.popup = document.getElementById("popup");
		this.select = document.getElementById("hoursSelect");
	}
	
	this.close = function()
	{
		this.isOpen = false;
		this.popup.style.display = "none";
		this.currentTimelog = null;
		this.currentElementTimelog = null;
		this.currentElementTime = null;
	}
	
	this.open = function(element,elementTime,timelog,folder,task)
	{
		if(this.isOpen && this.currentTimelog === timelog)
		{
			this.close();
			return;
		}
		this.currentTimelog = timelog;
		this.currentElementTimelog = element;
		this.currentElementTime = elementTime;
		
		var point = position(element,true);
		this.isOpen = true;
		
		document.getElementById("titlePopup").innerHTML = timelog.trackedDate + " " + (task ? task.title : "");
		
		if(!this.__containsHour(timelog.hours))
		{
			this.__addHour(timelog.hours);
		}
		
		this.__selectHour(timelog.hours);
		
		this.popup.style.top = point.top - 100+"px";
		this.popup.style.left = point.left - 50 +"px";
		this.popup.style.display = "block";
	}
	
	this.remove = function()
	{
		if(this.currentTimelog==null || this.currentElementTimelog==null || this.isOpen == false)
			return;
		
		if (confirm("Vous désirez vraiment supprimer ?")) {
			mhn.splash.wait();
			mhn.timelogs.remove(this.currentTimelog,
			function(){
				self.currentElementTimelog.parentElement.removeChild(self.currentElementTimelog);
				mhn.splash.wake();
				self.close();
			},
			function(error){
				mhn.splash.show(mhn.splash.ERROR, "Error remove timelog", 0, false);
			});
		}
	}
	
	this.update = function()
	{
		if(this.currentTimelog==null || this.currentElementTime==null || this.isOpen == false)
			return;
		
		this.currentTimelog.hours = this.select.value;
		mhn.splash.wait();
		mhn.timelogs.update(this.currentTimelog,
		function(){
			self.currentElementTime.innerHTML = self.select.value;
			mhn.splash.wake();
			self.close();
		},
		function(error){
			mhn.splash.show(mhn.splash.ERROR, "Error remove timelog", 0, false);
		});
	}
	
	this.__selectHour = function(hours)
	{
		this.select.value = hours;
	}
	
	this.__containsHour = function(hours)
	{
		return $("#hoursSelect option").filter(function() {
           return this.value == hours;
         }).length !== 0;
	}
	
	this.__addHour = function(hours)
	{
		var option = document.createElement( 'option' );
        option.value = option.text = hours;
        this.select.add( option );
	}
}