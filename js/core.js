mhn.core = new function()
{
	var self = this;
	this.isLoad = false;
	
	this.initialize = function()
	{
		var code = gup("code");
		if(!code){
			self.reload();
			return;
		}
		
		mhn.splash.progress(5);
		this.getCredentials();
		
		window.onresize = function(event) {
			$(self).trigger('onResize',event);
		};
	}
	
	this.reload = function()
	{
		window.location.replace("https://www.wrike.com/oauth2/authorize/v4?client_id=BSq9d5sq&response_type=code");
	}
	
	this.getCredentials = function()
	{
		//mhn.credentials.descriptor(null,null);
		
		mhn.credentials.get(
			function(){
				mhn.splash.wake();
				self.getContacts();
			},
			function(error){
				mhn.splash.stopProgress();
				mhn.splash.show(mhn.splash.WARN, error, 0, false, "Reload", function(){self.reload();});
			}
		);
	}
	
	this.getContacts = function()
	{
		mhn.contacts.get(
			function(){
				mhn.splash.wake();
				self.getTasks();
				//mhn.log.add(mhn.contacts.me.firstName+" "+mhn.contacts.me.lastName);
			},
			function(error){
				mhn.splash.stopProgress();
				mhn.splash.show(mhn.splash.ERROR, error, 0, false);
			}
		);
	}
	
	this.getTasks = function()
	{
		mhn.tasks.get(
			function(){
				mhn.splash.wake();
				self.getFolders();
			},
			function(error){
				mhn.splash.stopProgress();
				mhn.splash.show(mhn.splash.ERROR, error, 0, false);
			}
		);
	}
	
	this.getFolders = function()
	{
		mhn.folders.get(
			function(){
				mhn.splash.wake();
				self.getTimelogs();
			},
			function(error){
				mhn.splash.stopProgress();
				mhn.splash.show(mhn.splash.ERROR, error, 0, false);
			}
		);
	}
	
	this.getTimelogs = function()
	{
		mhn.timelogs.get(
			function(){
				mhn.splash.wake();
				self.afterLoad();
			},
			function(error){
				mhn.splash.stopProgress();
				mhn.splash.show(mhn.splash.ERROR, error, 0, false);
			}
		);
	}

	
	// ========================================================
	// All data loaded
	// ========================================================
	this.__loaded = false;
	this.__onLoadHandlers = [];
	this.onLoad = function(callback)
	{
		if( this.__loaded )
		{
			setTimeout(function()
			{
				try { callback(); } catch(e) { }
			}, 1);
		}
		else
			this.__onLoadHandlers.push(callback);
	}
	
	this.afterLoad = function()
	{
		this.__loaded = true;
		
		while( this.__onLoadHandlers.length > 0 )
		{
			var callback = this.__onLoadHandlers.shift();
			callback();
		}
		
		$(document).keydown(function(event) {
			self.keyCode = event.keyCode;
			mhn.status.log("keyup:"+self.keyCode,"ERROR");
		});
		
		$(document).keyup(function(event) {
			self.keyCode = "";
			mhn.status.log("keydown","ERROR");
		});
	}
}



/*****************************************************
 * Description:
 *		Hooks a one-shot callback on the ESC key press
 * Parameters:
 *		the callback
 * Returns:
 *		the hook to eventually disconnect it
 ****************************************************/
document.onEscape = function(callback, element)
{
	if( typeof element == 'string' ) element = document.getElementById(element);
	if( !element ) element = document;
	
	$(element).keyup(
		function(event)
		{
			event = event || window.event;
			if( event.keyCode == 27 || event.which == 27 )
			{
				$(element).off("keyup");
				callback(event);
			}
		}
	);
	
	///REMOVE WITH
	///$(element).off("keyup");
	return function(){$(element).off("keyup");};
}