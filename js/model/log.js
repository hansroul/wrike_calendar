mhn.log = new function()
{
	var self = this;
	
	this.add = function(message)
	{
		$.ajax({
			type: 'POST',
			url: 'rest.svc/log',
			contentType: "application/json",
            dataType: "json",
			data:JSON.stringify({
				"message":message,
			}),
			complete: function (resp) {
				mhn.status.log("Add log success","DONE");
			},
			error: function (jqXHR,  textStatus,  errorThrown) {
			  mhn.status.log("Add log Error:"+textStatus,"ERROR");
			}
		});
	}
}