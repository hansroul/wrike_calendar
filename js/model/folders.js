mhn.folders = new function()
{
	var self = this;
	this.rootTitles = ["JEUX HTML5 (JiM)","_Gaming HTML5 - Devs","_Back-End","Game Engine","_Gaming HTML5 - Game Server","Support"];
	this.folders;
	
	this.get = function(completeCallback,errorCallback)
	{
		this.allFolders = [];
		this.folders = [];
		$.ajax({
			type: 'GET',
			url: 'https://www.wrike.com/api/v4/folders?',
			data:{
				"access_token":mhn.credentials.accessToken,
				"fields": '["metadata","description","briefDescription"]'
			},
			complete: function (resp) {
				var data = JSON.parse(resp.responseText).data;
				for(var i=0;i<data.length;i++){
					if(data[i].scope == "RbFolder") continue;
					self.folders.push(data[i]);
				}
				
				for(var i=0;i<self.folders.length;i++){
					self.__addFolderTasks(self.folders[i]);
				}
				
				if(completeCallback)
					completeCallback();
			},
			error: function (jqXHR,  textStatus,  errorThrown) {
			  mhn.status.log("Get folders Error:"+textStatus,"ERROR");
			  if(errorCallback)
				errorCallback(textStatus);
			}
		});
	}
	
	this.__addFolderTasks = function(folder)
	{
		var tasks = [];
		for(var i=0;i<mhn.tasks.tasks.length;i++)
		{
			for(var p=0;p<mhn.tasks.tasks[i].parentIds.length;p++)
			{
				if(mhn.tasks.tasks[i].parentIds[p]==folder.id)
				{
					tasks.push(mhn.tasks.tasks[i]);
				}
			}
		}
		
		if(tasks){
			if(folder.tasks)
				folder.tasks = folder.tasks.concat(tasks);
			else
				folder.tasks = tasks;
		}
	}
	
	this.getFolderByTitle = function(title)
	{
		for(var i=0;i<this.folders.length;i++)
		{
			if(this.folders[i].title == title)
			{
				return this.folders[i];
			}
		}
		
		return null;
	}
	
	this.getFolderById = function(id)
	{
		for(var i=0;i<this.folders.length;i++)
		{
			if(this.folders[i].id == id)
			{
				return this.folders[i];
			}
		}
		
		return null;
	}
}