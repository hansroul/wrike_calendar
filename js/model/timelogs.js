mhn.timelogs = new function()
{
	var self = this;
	this.timelogs;
	
	this.get = function(completeCallback,errorCallback)
	{
		this.timelogs = [];
		$.ajax({
			type: 'GET',
			url: 'https://www.wrike.com/api/v4/timelogs?',
			data:{
				"access_token":mhn.credentials.accessToken,
				"me":true
			},
			complete: function (resp) {
				
				var data = JSON.parse(resp.responseText).data;
				for(var i=0;i<data.length;i++){
					self.timelogs.push(data[i]);
				}
				
				if(completeCallback)
					completeCallback();
			},
			error: function (jqXHR,  textStatus,  errorThrown) {
			  mhn.status.log("Get timelogs Error:"+textStatus,"ERROR");
			  if(errorCallback)
				errorCallback(textStatus);
			}
		});
	}
	
	this.getTimeLogs = function(date)
	{	
		var timeLogs = [];
		for(var i=0;i<this.timelogs.length;i++)
		{
			var createdDate = this.timelogs[i].trackedDate.substring(0,10);
			var parametersDate = this.convertDate(date);
			if(createdDate==parametersDate)
				timeLogs.push(this.timelogs[i]);
		}
		return timeLogs;
	}
	
	this.getTimeLogIndex = function(timelog)
	{
		for(var i=0;i<this.timelogs.length;i++)
		{
			if(this.timelogs[i].id == timelog.id)
				return i;
		}
		return null;
	}
	
	this.convertDate = function(d) {
	  function pad(s) { return (s < 10) ? '0' + s : s; }
	  return [d.getFullYear(), pad(d.getMonth()+1), pad(d.getDate())].join('-');
	}
	
	this.remove = function(timelog,completeCallback,errorCallback)
	{		
		$.ajax({
			type: 'DELETE',
			url: 'https://www.wrike.com/api/v4/timelogs/'+timelog.id+'?access_token='+mhn.credentials.accessToken,
			complete: function (resp) {
				var index = self.getTimeLogIndex(timelog);
				self.timelogs.splice(index, 1);
				
				if(completeCallback)
					completeCallback();
			},
			error: function (jqXHR,  textStatus,  errorThrown) {
			  mhn.status.log("Remove timelogs Error:"+textStatus,"ERROR");
			  if(errorCallback)
				errorCallback(textStatus);
			}
		});
	}
	
	this.add = function(timelog,completeCallback,errorCallback)
	{
		$.ajax({
			type: 'POST',
			url: 'https://www.wrike.com/api/v4/tasks/'+timelog.taskId+'/timelogs?',
			data:{
				"access_token":mhn.credentials.accessToken,
				"comment":timelog.comment,
				"hours":timelog.hours,
				"trackedDate":timelog.trackedDate
			},
			complete: function (resp) {
				var data = JSON.parse(resp.responseText).data;
				timelog.id = data[0].id;
				self.timelogs.push(timelog);
				if(completeCallback)
					completeCallback();
			},
			error: function (jqXHR,  textStatus,  errorThrown) {
			  mhn.status.log("Add timelogs Error:"+textStatus,"ERROR");
			  if(errorCallback)
				errorCallback(textStatus);
			}
		});
	}
	
	this.update = function(timelog,completeCallback,errorCallback)
	{
		$.ajax({
			type: 'PUT',
			url: 'https://www.wrike.com/api/v4/timelogs/'+timelog.id+'?',
			data:{
				"access_token":mhn.credentials.accessToken,
				"comment":timelog.comment,
				"hours":timelog.hours,
				"trackedDate":timelog.trackedDate
			},
			complete: function (resp) {
				if(completeCallback)
					completeCallback();
			},
			error: function (jqXHR,  textStatus,  errorThrown) {
			  mhn.status.log("Add timelogs Error:"+textStatus,"ERROR");
			  if(errorCallback)
				errorCallback(textStatus);
			}
		});
	}
}