mhn.tasks = new function()
{
	var self = this;
	
	this.tasks;
	this._status = [
		{"name":"Backlogged","color":"rgb(33, 150, 243)"},
		{"name":"In Progress","color":"rgb(0, 188, 212)"},
		{"name":"In Testing","color":"rgb(255, 152, 0)"},
		{"name":"Ready to deploy","color":"rgb(255, 235, 59)"},
		{"name":"Blocked","color":"rgb(233, 30, 99)"},
		{"name":"Completed","color":"rgb(139, 195, 74)"},
		{"name":"Deferred","color":"rgb(103, 58, 183)"},
		{"name":"Cancelled","color":"rgb(158, 158, 158)"}
	]
	
	this.get = function(completeCallback,errorCallback)
	{
		this.tasks = [];
		$.ajax({
			type: 'GET',
			url: 'https://www.wrike.com/api/v4/tasks?',
			data:{
				"access_token":mhn.credentials.accessToken,
				"fields": '["metadata","description","briefDescription","authorIds","parentIds","subTaskIds"]',
				"subTasks": true,
				"responsibles": '["'+mhn.contacts.me.id+'"]'
			},
			complete: function (resp) {
				
				var data = JSON.parse(resp.responseText).data;
				for(var i=0;i<data.length;i++){
					if(data[i].scope == "RbTask") continue;
					self.tasks.push(data[i]);
				}
				
				if(completeCallback)
					completeCallback();
			},
			error: function (jqXHR,  textStatus,  errorThrown) {
			  mhn.status.log("Get tasks Error:"+textStatus,"ERROR");
			  if(errorCallback)
				errorCallback(textStatus);
			}
		});
	}
	
	this.getStatus= function(task){
		if(!task || !task.status) return null;
		for(var i=0;i<this._status.length;i++){
			if(this._status[i].name.toUpperCase() == task.status.toUpperCase())
				return this._status[i];
		}
		return null;
	}
	
	this.getTaskById = function(id)
	{
		for(var i=0;i<this.tasks.length;i++)
		{
			if(this.tasks[i].id == id)
			{
				return this.tasks[i];
			}
		}
		
		return null;
	}
}