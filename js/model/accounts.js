mhn.accounts = new function()
{
	var self = this;
	this.accounts;
	
	this.get = function(completeCallback,errorCallback)
	{
		self.accounts = [];
		$.ajax({
			type: 'GET',
			url: 'https://www.wrike.com/api/v4/accounts?fields=["subscription"]&access_token='+mhn.credentials.accessToken,
			complete: function (resp) {
				var data = JSON.parse(resp.responseText).data;
				for(var i=0;i<data.length;i++){
					self.accounts.push(data[i]);
				}
				
				if(completeCallback)
					completeCallback();
			},
			error: function (jqXHR,  textStatus,  errorThrown) {
			  mhn.status.log("Get accounts Error:"+textStatus,"ERROR");
			  if(errorCallback)
				errorCallback(textStatus);
			}
		});
	}
}