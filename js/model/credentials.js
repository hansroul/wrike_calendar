mhn.credentials = new function()
{
	var self = this;
	this.accessToken;
	this.refreshToken;
	this.tokenType;
	this.expiresIn;
	this.code;
	
	this.descriptor = function(completeCallback,errorCallback)
	{
		$.get("http://games.gaming1.com/winning_games_api/v1/game-winnings/spin?alt=json",
        function(data, textStatus)
        {
			if(completeCallback)
				completeCallback();
        })
		.fail(function(xhr, status, error) {
			try
			{
				if(xhr.responseText && xhr.responseText!="")
				{
					var response = JSON.parse(xhr.responseText);
					mhn.status.log(response.error+ ":" + response.error_description,"ERROR");
					if(errorCallback)
						errorCallback(response.error_description);
				}
				else
				{
					if(errorCallback)
						errorCallback("Descriptor ERROR. Please Reload.");
				}
			}
			catch(err) {
				if(errorCallback)
					errorCallback("Descriptor ERROR. Please Reload.");
			}
		});
	}
	
	this.get = function(completeCallback,errorCallback)
	{
		this.code = gup("code");
		if(!this.code){
			mhn.status.log("Wrike code is don't exist in url !!!","ERROR");
			return;
		}
		
		$.post("https://www.wrike.com/oauth2/token",
        {
            client_id:"BSq9d5sq",
			client_secret:"gLl79XYBiNzUtYoOOJFfLqhN8QRRbONGACutaye6g1z01EG3YTB48XjBKU8KrQ35",
			grant_type:"authorization_code",
			code:this.code
        },
        function(data, textStatus)
        {
			self.accessToken = data.access_token;
			self.refreshToken = data.refresh_token;
			self.tokenType = data.token_type;
			self.expiresIn = data.expires_in;
			if(completeCallback)
				completeCallback();
        })
		.fail(function(xhr, status, error) {
			try
			{
				if(xhr.responseText && xhr.responseText!="")
				{
					var response = JSON.parse(xhr.responseText);
					mhn.status.log(response.error+ ":" + response.error_description,"ERROR");
					if(errorCallback)
						errorCallback(response.error_description);
				}
				else
				{
					if(errorCallback)
						errorCallback("Get credentials ERROR. Please Reload.");
				}
			}
			catch(err) {
				if(errorCallback)
					errorCallback("Get credentials ERROR. Please Reload.");
			}
		});
	}
	
	this.refresh = function()
	{
		$.post("https://www.wrike.com/oauth2/token",
        {
            client_id:"BSq9d5sq",
			client_secret:"gLl79XYBiNzUtYoOOJFfLqhN8QRRbONGACutaye6g1z01EG3YTB48XjBKU8KrQ35",
			grant_type:"refresh_token",
			refresh_token:this.refreshToken
        },
        function(data, textStatus)
        {
			self.accessToken = data.acces_token;
			self.refreshToken = data.refresh_token;
			self.tokenType = data.token_type;
			self.expiresIn = data.expires_in;
        })
		.fail(function(xhr, status, error) {
			mhn.status.log("Refresh credentials error: " + status,"ERROR");
		});
	}
}