mhn.contacts = new function()
{
	var self = this;
	this.contacts;
	
	this.get = function(completeCallback,errorCallback)
	{
		self.contacts = [];
		$.ajax({
			type: 'GET',
			url: 'https://www.wrike.com/api/v4/contacts?me=true&access_token='+mhn.credentials.accessToken,
			complete: function (resp) {
				var data = JSON.parse(resp.responseText).data;
				for(var i=0;i<data.length;i++){
					self.contacts.push(data[i]);
					self.me = data[i];
				}
				
				if(completeCallback)
					completeCallback();
			},
			error: function (jqXHR,  textStatus,  errorThrown) {
			  mhn.status.log("Get contacts Error:"+textStatus,"ERROR");
			  if(errorCallback)
				errorCallback(textStatus);
			}
		});
	}
}