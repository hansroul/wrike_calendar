/*

// =============================
// SAMPLE USAGE
// =============================

mhn.core.onLoad(function()
{
	var itemProp = {
		'data': {}, // put anything you want in here
		'children': [], // sub elements of this entry
		'openable': true, // whether the entry is forced to be openable or forced not to be
		'open': true, // start open or not
		'content': '', // any HTML or DOM that will be used as label
		'dragable': true // whether this entry can be dragged and dropped
	};
	
	var tree = new mhn.Tree('treeContainer', [
		new mhn.TreeItem({'data': {}, 'content':'<input type="checkbox" />Building numbers</span>', 'open': false, 'children': [
			new mhn.TreeItem({'dragable': false, 'content':'<div style="display: inline-block; width: 25px; height: 20px; border: 1px solid red; background-color: #FF7777;"></div> Legend 1'}),
			new mhn.TreeItem({'dragable': false, 'content':'<div style="display: inline-block; width: 25px; height: 20px; border: 1px solid green; background-color: #77FF77;"></div> Legend 2'}),
			new mhn.TreeItem({'dragable': false, 'content':'<div style="display: inline-block; width: 25px; height: 20px; border: 1px solid blue; background-color: #7777FF;"></div> Legend 3'})
		]})
	]);
	
});

*/

mhn.properties.tree = {
	allowDragAccrossGroup: false,
	uid: 0
};

mhn.Tree = function(div, items)
{
	var self = this;
	this.ROOT = new mhn.TreeItem({'data': this, 'openable': true, 'open': true, 'dragable': false});
	
	if( typeof div == 'string' )
		div = document.getElementById(div);
		
	removeClassName(this.ROOT.dom, 'treeItem');
	div.appendChild(this.ROOT.dom);
	
	if( typeof items != 'undefined' )
		this.ROOT.addChild(items);
	
	this.onDragDrop = function(item, parent){}
	this.onAddChild = function(item){}
	this.onRemoveChild = function(item){}
	this.onClear = function(){}
	this.onMoveChild = function(item){}
	
	$(this.ROOT).bind("onAddChild",function(item) { self.onAddChild(item); });
	$(this.ROOT).bind('onRemoveChild', function(item) { self.onRemoveChild(item); });
	$(this.ROOT).bind('onClear', function(item) { self.onClear(item); });
	$(this.ROOT).bind('onMoveChild', function(item) { self.onMoveChild(item); });
	
	this.addChild = function(child) { this.ROOT.addChild(child); }
	this.removeChild = function(child) { this.ROOT.removeChild(child); }
	this.clear = function() { this.ROOT.clear(); }
	this.items = function() { return this.ROOT.children; }
	
	this.find = function(property, value, parent)
	{
		if( !parent )
			parent = this.ROOT;
		
		if( typeof property == 'string' )
			property = property.split('.');
		
		var data = parent.data;
		for( var p = 0; p < property.length; p++ )
			if(data)
				data = data[property[p]];
		
		if( data == value )
			return parent;

		for( var i = 0; i < parent.children.length; i++ )
		{
			var item = this.find(property, value, parent.children[i]);
			if( item )
				return item;
		}
		
		return null;
	}
	
	// =============================
	// JSON
	// =============================
	this.toJSON = function()
	{
		return this.items();
	}
	
	this.fromJson = function(json)
	{
		this.clear();
		
		for( var i = 0; i < json.length; i++ )
		{
			var item = new mhn.TreeItem();
			item.fromJson(json[i]);
			this.addChild(item);
		}
	}
}

mhn.TreeItem = function(properties)
{
	var self = this;
	this.data = null;
	this.dom = null;
	this.parent = null;
	this.children = [];
	
	this.onAddChild = function(item){$(self).trigger('onAddChild',item);}
	this.onRemoveChild = function(item){$(self).trigger('onRemoveChild',item);}
	this.onClear = function(){$(self).trigger('onClear');}
	this.onMoveChild = function(item){$(self).trigger('onMoveChild');}
	this.onRemove = function(){$(self).trigger('onRemove');}
	
	// =============================
	// INITIALIZE DOM NODE
	// =============================
	this.initialize = function()
	{
		this.dom = document.createElement('DIV');
		this.dom.className = "treeItem level_" + this.getLevel();
		this.dom.id = '__ti_' + mhn.properties.tree.uid++;
		this.dom.getItem = function() { return self; }
		
		$(this.dom).click(function(e){
			e = e || window.event;
			var t = e.target || e.srcElement;
			if( t === self.dom )
				self.toggle();
		});
		
		// content node
		var content = document.createElement('DIV');
		content.className = "treeItemLabel";
		$(content).mousedown(function(){
		});
		this.dom.appendChild(content);
		
		// children node
		var children = document.createElement('DIV');
		children.className = "treeItemChildrenContainer";
		this.dom.appendChild(children);
		
		// open by default
		this.open();
	}
	
	this.setContent = function(content)
	{
		if( typeof content == 'string' )
			this.dom.childNodes[0].innerHTML = content;
		else
			this.dom.childNodes[0].appendChild(content);
	}
	
	// =============================
	// OPEN/CLOSE
	// =============================
	this.setOpenable = function(value)
	{
		if( value )
			addClassName(this.dom, 'openable');
		else
			removeClassName(this.dom, 'openable');
	}
	
	this.isOpenable = function() { return hasClassName(this.dom, 'openable'); }
	this.isOpen = function() { return hasClassName(this.dom, 'open'); }
	this.isClosed = function() { return !this.isOpen(); }
	
	this.open = function()
	{
		if( !this.isOpenable() )
			return;
		addClassName(this.dom, 'open');
	}
	
	this.close = function()
	{
		if( !this.isOpenable() )
			return;
		removeClassName(this.dom, 'open');
	}
	
	this.toggle = function()
	{
		if( this.isOpen() )
			this.close();
		else
			this.open();
	}
	
	// =============================
	// LEVEL
	// =============================
	this.getLevel = function()
	{
		var level = 0;
		var parent = this.parent;
		while( parent != null )
		{
			level++;
			parent = parent.parent;
		}
		
		return level;
	}
	
	// =============================
	// CHILDREN
	// =============================
	this.addChild = function(child)
	{
		if( child instanceof mhn.TreeItem )
			child = [child];

		for( var i = 0; i < child.length; i++ )
		{
			if( !(child[i] instanceof mhn.TreeItem) )
				continue;

			child[i].parent = this;
			this.dom.childNodes[1].appendChild(child[i].dom);
			this.children.push(child[i]);
			
			this.onAddChild(child[i]);
		}
		
		this.setOpenable( this.isOpenable() || (this.children.length > 0));
	}
		
	this.removeChild = function(child)
	{
		if( child instanceof mhn.TreeItem )
		{
			for( var i = 0; i < this.children.length; i++ )
			{
				if( this.children[i] === child )
				{
					child = i;
					break;
				}
			}
		}
		
		if( typeof child != 'number' || child < 0 || child >= this.children.length )
			throw 'Invalid child item';
		
		var original = this.children.splice(child, 1);
		this.dom.childNodes[1].removeChild(this.dom.childNodes[1].childNodes[child]);
		
		original[0].onRemove();
		this.onRemoveChild(original[0]);
	}
	
	this.clear = function()
	{
		for( var i = 0; i < this.children.length; i++ )
		{
			this.children[i].clear();
			this.children[i].onRemove();
		}
		
		this.children = [];
		while( this.dom.childNodes[1].childNodes.length > 0 )
		{
			this.dom.childNodes[1].removeChild(this.dom.childNodes[1].firstChild);
		}
			
		this.onClear();
	}
	
	// =============================
	// INDEX
	// =============================
	this.getIndex = function()
	{
		if( this.parent == null )
			return -1;
		else
		{
			var sibblings = this.parent.children;
			for( var i = 0; i < sibblings.length; i++ )
				if( sibblings[i] === this )
					return i;
			return -1;
		}
	}
	
	this.setIndex = function(index)
	{
		try
		{
			if( this.parent == null )
				throw 'Impossible to set item index because no parent element is defined';
			if( index < 0 )
				throw 'Invalid index';
			
			var current = this.getIndex();
			if( index == current )
				return;

			if( index < current )
			{
				this.parent.children.splice(current, 1);
				this.parent.children.splice(index, 0, this);
				this.parent.dom.childNodes[1].insertBefore(this.parent.dom.childNodes[1].childNodes[current], this.parent.dom.childNodes[1].childNodes[index]);
			}
			else
			{
				this.parent.children.splice(current, 1);
				this.parent.children.splice(index, 0, this);
				this.parent.dom.childNodes[1].insertBefore(this.parent.dom.childNodes[1].childNodes[current], this.parent.dom.childNodes[1].childNodes[index+1]);
			}
			
			this.parent.onMoveChild(this);
		}
		catch(e)
		{
			mhn.status.log("TreeItem setIndex:"+e,"ERROR");
		}
	}
	
	// =============================
	// DRAGABLE
	// =============================
	this.dragable = true;
	
	// =============================
	// CONSTRUCTOR
	// =============================
	this.initialize();
	if( typeof properties == 'object' )
	{
		if( properties.hasOwnProperty('data') )
			this.data = properties.data;
		
		if( properties.hasOwnProperty('children') )
			this.addChild(properties.children);

		if( properties.hasOwnProperty('openable') )
			this.setOpenable(properties.openable);
		if( properties.hasOwnProperty('open') )
		{
			if( properties.open )
				this.open();
			else
				this.close();
		}
		if( properties.hasOwnProperty('content') )
			this.setContent(properties.content);
	}

	// =============================
	// JSON
	// =============================
	this.toJSON = function()
	{
		return {
			'data': this.data,
			'children': this.children,
			'dragable': this.dragable,
			'content': this.dom.childNodes[0].innerHTML
		};
	}
	
	this.fromJson = function(json)
	{
		this.clear();
		this.data = null;
		
		if( json.hasOwnProperty('data') )
			this.data = json.data;
		if( json.hasOwnProperty('children') && json.children )
		{
			for( var i = 0; i < json.children.length; i++ )
			{
				var item = new mhn.TreeItem();
				item.fromJson(json.children[i]);
				this.addChild(item);
			}
		}
		if( json.hasOwnProperty('content') )
			this.setContent(json.content);
	}
}