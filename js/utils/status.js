mhn.status = new function()
{
	var self = this;
	this.SEVERE 	= 1;
	this.WARNING 	= 10;
	this.INFO 		= 100;
	this.CONFIG 	= 1000;
	this.FINE 		= 10000;
	this.FINER 		= 100000;
	this.FINEST 	= 1000000;
	
	this.log = function(message, level)
	{
		if( typeof message == 'undefined' || !message || message.length == 0 )
			return;
			
		level = this._parseLevel(level);
		
		if( level > this._level )
			return;
		
		console.log(this._levelToString(level) + ' >> ' + message);
	}
	
	// ========================
	// LEVEL (only messages <= level will be considered)
	// ========================
	this._level = this.INFO;
	this.setLevel = function(level)
	{
		var previous = this._level;
		this._level = this._parseLevel(level);
		return previous;
	}
	
	this.getLevel = function()
	{
		return this._level;
	}
	
	this._parseLevel = function(level)
	{
		if( typeof level == 'string' )
		{
			switch(level.toLowerCase())
			{
				case 'severe':
				case 'error': level = this.SEVERE; break;
				case 'warn':
				case 'warning': level = this.WARNING; break;
				case 'wait': level = this.INFO+1; break;
				case 'done': level = this.INFO+2; break;
				default:
				case 'info': level = this.INFO; break;
				case 'config': level = this.CONFIG; break;
				case 'fine':
				case 'debug':
				case 'bug': level = this.FINE; break;
				case 'finer': level = this.FINER; break;
				case 'finest': level = this.FINEST; break;
			}
		}
		if( isNaN(parseInt(level)) || !isFinite(level) || level < 0 )
			level = this.INFO;
			
		return level;
	}

	this._levelToString = function(level)
	{
		if( typeof level == 'string' )
			return level;
		
		switch(level)
		{
			case this.SEVERE: return 'SEVERE';
			case this.WARNING: return 'WARNING';
			case this.INFO+2: return 'DONE';
			case this.INFO+1: return 'WAIT';
			case this.INFO: return 'INFO';
			case this.CONFIG: return 'CONFIG';
			case this.FINE: return 'FINE';
			case this.FINER: return 'FINER';
			case this.FINEST: return 'FINEST';
			default: return '(' + level + ')';
		}
	}
	
	// ========================
	// CONSOLE
	// ========================
	if( !window.console )
	{
		window.console = { log: function(){} };
	}
}