mhn.properties.splash = {delay: 500};

mhn.splash = new function()
{
	var self = this;
	this.SUCCESS 	= 1;
	this.ERROR 		= 2;
	this.WAIT 		= 3;
	this.INFO 		= 4;
	this.WARN 		= 5;

	this.__buttonCallback;	
	
	this.initialize = function()
	{
		var shade = document.createElement("DIV");
		var align = document.createElement("DIV");
		var content = document.createElement("DIV");
		var sizer = document.createElement("DIV");
		var text = document.createElement("DIV");
		var body = document.getElementsByTagName('body')[0];
		var button = document.createElement("A");
		
		shade.id = 'splash_shade';
		align.id = 'splash_align';
		content.id = 'splash_content';
		sizer.id = 'splash_sizer';
		text.id = 'splash_text';
		button.id = 'splash_button';
		
		button.className = 'button';
		
		$(button).click(function(){
			if(self.__buttonCallback)
				self.__buttonCallback();
		});
		
		content.appendChild(sizer);
		content.appendChild(text);
		content.appendChild(button);
		
		align.appendChild(content);
		body.insertBefore(align, body.firstChild);
		body.insertBefore(shade, body.firstChild);
		this.hide();
	}
	
	this.flash = function(status)
	{
		this.show(status, null, mhn.properties.splash.delay, false);
	}
	
	this.show = function(status, message, delay, shade, buttonLabel, buttonCallback)
	{
		this.__buttonCallback = buttonCallback;
		if(buttonCallback){
			document.getElementById("splash_button").innerHTML = buttonLabel;
			show("splash_button");
		}
		
		// do not do anything when progress is in use
		if( this.__inProgress )
			return;
			
		if( this.__timeoutHandle != null )
		{
			clearTimeout(this.__timeoutHandle);
			this.__timeoutHandle = null;
		}

		this.__waitMax = 0;
		this.__waitCurrent = 0;

		if( typeof status != 'number' )
		{
			mhn.status.log("Unexpected status type for splash.show()", mhn.status.FINEST);
			return;
		}
		
		if( typeof message == 'string' )
			document.getElementById("splash_text").innerHTML = message;
		else
			document.getElementById("splash_text").innerHTML = '';
		
		if( typeof shade == 'undefined' || shade )
			show("splash_shade");
		removeClassName("splash_align", "error");
		removeClassName("splash_align", "success");
		removeClassName("splash_align", "wait");
		removeClassName("splash_align", "info");
		removeClassName("splash_align", "warn");
		
		switch( status )
		{
			case this.SUCCESS: addClassName("splash_align", "success"); break;
			case this.ERROR: addClassName("splash_align", "error"); break;
			case this.WAIT: addClassName("splash_align", "wait"); break;
			case this.INFO: addClassName("splash_align", "info"); break;
			case this.WARN: addClassName("splash_align", "warn"); break;
		}

		show("splash_align");
		
		if( status != this.WAIT )
		{
			document.onEscape(function() { self.hide(); });
		}
		
		if( typeof delay == 'number' && delay > 0 )
			this.__timeoutHandle = setTimeout(function() { self.hide(); }, delay);
	}
	
	this.hide = function()
	{
		// do not do anything when progress is in use
		if( this.__inProgress )
			return;

		hide("splash_shade");
		hide("splash_align");
		hide("splash_button");
		
		if( this.__timeoutHandle != null )
		{
			clearTimeout(this.__timeoutHandle);
			this.__timeoutHandle = null;
		}
	}
	
	this.__timeoutHandle = null;
	this.__waitMax = 0;
	this.__waitCurrent = 0;
	this.__inProgress = false;
	
	this.wait = function(shade)
	{
		if( this.__waitMax == 0 )
			this.show(this.WAIT, null, 0, shade);
		this.__waitMax++;
	}
	
	this.wake = function(silent)
	{
		if( this.__inProgress )
		{
			this.progress();
			return;
		}
		
		this.__waitCurrent++;
		
		if( this.__waitCurrent >= this.__waitMax )
		{
			this.__waitCurrent = 0;
			this.__waitMax = 0;
			this.__inProgress = false;
			if( !silent )
				this.show(this.SUCCESS, null, mhn.properties.splash.delay, true);
			else
				this.hide();
		}
	}
	
	this.stopProgress = function()
	{
		this.__waitCurrent = 0;
		this.__waitMax = 0;
		this.__inProgress = false;
	}
	
	this.progress = function(max)
	{
		if( typeof max == 'undefined' )
			this.__waitCurrent++;
		else
		{
			if( max <= 0 )
				return;
				
			max += this.__waitMax;
			var current = this.__waitCurrent;
			
			// this will set __waitMax and __waitCurrent = 0
			// so we must backup the values before and re-set them after
			this.show(this.WAIT);
			this.__waitMax = max;
			this.__waitCurrent = current;
			this.__inProgress = true;
		}

		if( this.__waitCurrent >= this.__waitMax )
		{
			setTimeout(function()
			{
				self.__inProgress = false;
				if(self.__waitCurrent >= self.__waitMax)
				{
					self.__waitMax = 0;
					self.__waitCurrent = 0;
					self.hide();
				}				
			}, mhn.properties.splash.delay);
		}

		var ratio = this.__waitCurrent / this.__waitMax;
		var p = "<div id=\"splash_progress_outer\">" + 
				"<div id=\"splash_progress_inner\" style=\"width: " +  Math.ceil(ratio * 250) + "px;\"></div>" + 
				"</div><br />" + Math.ceil(ratio * 100) + "%";
		document.getElementById("splash_text").innerHTML = p;
	}
	
	this.initialize();
}
